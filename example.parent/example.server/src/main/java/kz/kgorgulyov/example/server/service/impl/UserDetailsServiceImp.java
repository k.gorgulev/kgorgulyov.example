package kz.kgorgulyov.example.server.service.impl;

import kz.kgorgulyov.example.server.dao.UserDao;
import kz.kgorgulyov.example.server.service.PasswordEncoder;
import kz.kgorgulyov.example.server.service.UserRoleService;
import kz.kgorgulyov.example.server.util.security.MyUserPrincipal;
import kz.kgorgulyov.example.share.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class UserDetailsServiceImp implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserRoleService userRoleService;


    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userDao.selByLogin(username);
        if (user == null) {
            throw new UsernameNotFoundException("");
        }
        user.permissions = userRoleService.getRolePermission(user.role);
        return new MyUserPrincipal(user);
    }
}