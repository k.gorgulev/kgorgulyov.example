package kz.kgorgulyov.example.share.model.user.form;


import kz.kgorgulyov.example.share.model.user.User;
import kz.kgorgulyov.example.share.util.base.BaseForm;

public class UserRegistrationForm extends BaseForm {


    public String login;
    public String password;
    public String password_confirm;


    public String name;
    public String surname;


    @Override
    public void validateForm() {
        validateStr("login", login);
        validateStr("surname", surname);
        if (validateStr("password", password)) {
            if (validateStr("password_confirm", password_confirm)) {
                if (!password.equals(password_confirm)) {
                    validationErrors.add("password is not equal password_confirm");
                }
            }
        }

    }

    public User toUser() {
        User u = new User();
        u.login = login;
        u.password = password;
        u.name = name;
        u.surname = surname;


        return u;

    }
}

