create table g_setting
(
    setting     varchar(255),
    val         text,
    server_name text default 'default_host',
    primary key (setting, server_name)
);