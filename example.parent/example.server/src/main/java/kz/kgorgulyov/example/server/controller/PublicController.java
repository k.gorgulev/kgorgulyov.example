package kz.kgorgulyov.example.server.controller;

import kz.kgorgulyov.example.server.service.AuthService;
import kz.kgorgulyov.example.server.service.UserService;
import kz.kgorgulyov.example.server.util.security.MyUserPrincipal;
import kz.kgorgulyov.example.share.exception.InvalidFormException;
import kz.kgorgulyov.example.share.model.user.dto.UserDto;
import kz.kgorgulyov.example.share.model.user.form.UserLoginForm;
import kz.kgorgulyov.example.share.model.user.form.UserRegistrationForm;
import kz.kgorgulyov.example.share.util.CommonUtil;
import kz.kgorgulyov.example.share.util.rest.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static kz.kgorgulyov.example.share.util.rest.RestResponse.NOT_FOUND;

@RestController
@RequestMapping("/public")
public class PublicController {


    @Autowired
    public UserService userService;

    @Autowired
    public AuthService authService;



    @RequestMapping(path = "/user", method = RequestMethod.POST)
    public RestResponse create(@RequestBody UserRegistrationForm form) throws InvalidFormException {
        form.validate();
        return RestResponse.ok().addData(userService.create(form.toUser()));

    }


    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public RestResponse login(@RequestBody UserLoginForm form, HttpServletRequest req) throws InvalidFormException {
        form.validate();

        Authentication auth = authService.auth(req.getSession(true), form.login, form.password);

        if (auth != null && CommonUtil.bool(auth.isAuthenticated())) {
            return RestResponse.ok().addData(new UserDto(((MyUserPrincipal) auth.getPrincipal()).getUserAccount()));
        } else {
            return RestResponse.create(NOT_FOUND);
        }

    }


}
