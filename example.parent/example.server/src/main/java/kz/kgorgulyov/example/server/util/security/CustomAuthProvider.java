package kz.kgorgulyov.example.server.util.security;

import kz.kgorgulyov.example.server.service.UserService;
import kz.kgorgulyov.example.share.model.RolePermission;
import kz.kgorgulyov.example.share.model.user.User;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class CustomAuthProvider implements AuthenticationProvider {

    private UserService userService;

    public Logger logger = Logger.getLogger(CustomAuthProvider.class);

    public CustomAuthProvider(UserService userService) {
        this.userService = userService;
    }


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String name = authentication.getName();
        Object credentials = authentication.getCredentials();
        if (!(credentials instanceof String)) {
            return null;
        }
        String password = credentials.toString();
        User login = userService.login(name, password);
        if (login != null) {
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_" + login.role.name()));
            for (RolePermission permission : login.permissions) {
                grantedAuthorities.add(new SimpleGrantedAuthority(permission.name()));
            }

            return new PasswordAuthentication(name, password, grantedAuthorities, new MyUserPrincipal(login));
        }
        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }


    public void log(String msg) {
        logger.info(msg);
    }
}
