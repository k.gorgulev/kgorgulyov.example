package kz.kgorgulyov.example.server.util.security;

import kz.kgorgulyov.example.share.model.RolePermission;
import kz.kgorgulyov.example.share.model.user.User;
import org.springframework.security.core.context.SecurityContextHolder;

public class SessionContext {

    public static User getUserAccount() {

        MyUserPrincipal context = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        return context.getUserAccount();
    }

    public static Long userId() {

        MyUserPrincipal context = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        return context.getUserAccount().user;
    }


    public static boolean havePermission(RolePermission documentTemplateView) {
        return getUserAccount().permissions.contains(documentTemplateView);
    }
}