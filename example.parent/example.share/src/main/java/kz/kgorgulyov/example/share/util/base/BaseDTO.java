package kz.kgorgulyov.example.share.util.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import kz.kgorgulyov.example.share.util.DateUtils;

import java.time.LocalDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseDTO {

    public String date(LocalDateTime d) {
        return DateUtils.date2YearString(d);
    }
}
