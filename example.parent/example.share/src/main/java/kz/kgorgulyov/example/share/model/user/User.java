package kz.kgorgulyov.example.share.model.user;

import kz.kgorgulyov.example.share.model.Role;
import kz.kgorgulyov.example.share.model.RolePermission;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class User implements Serializable {

    public Long user;

    public String login;
    public String password;
    public String salt;
    public Role role;

    public Boolean active;

    public String name;
    public String surname;

    public List<RolePermission> permissions = new ArrayList<>();

    public LocalDateTime createdAt = LocalDateTime.now();


}

