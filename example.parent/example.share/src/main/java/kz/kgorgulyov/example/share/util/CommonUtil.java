package kz.kgorgulyov.example.share.util;

import org.apache.commons.lang3.StringUtils;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

public class CommonUtil {


    /**
     * @param phone в формате 7XXXXXXXXX
     * @return
     */

    public static List<String> KZ_CODES = Arrays.asList("700", "701", "702", "703", "703", "704", "705", "706", "707",
                                                        "708", "709", "747", "750", "751", "760", "761", "762", "763",
                                                        "764", "771", "775", "776", "775", "777", "778"

    );

    public static boolean isPhoneValid(String phone) {
        if (phone == null || phone.isEmpty()) { return false; }
        if (!phone.startsWith("77")) { return false; }
        if (!KZ_CODES.contains(phone.substring(1, 4))) { return false; }
        return true;

    }

    public static String clearPhone(String phone) {
        phone = phone.replaceAll("[^0-9]", "");
        if (phone.length() == 10) {
            phone = '7' + phone;
        } else if (phone.length() == 11 && phone.startsWith("8")) {
            phone = '7' + phone.substring(1);
        }
        return phone;
    }

    public static Boolean bool(String bool) {
        try {
            return Boolean.parseBoolean(bool);
        } catch (Exception e) {
            return false;
        }


    }

    public static boolean bool(Boolean bool) {
        return bool == null ? false : bool.booleanValue();
    }

    public static String nvl(String val) {
        if (val == null) {
            return "";
        }
        return val;
    }

    public static String escapeSql(String str) {
        if (str == null) {
            return null;
        }
        return StringUtils.replace(str, "'", "''");
    }

    public static String getSalt() {
        return new String(Base64.getEncoder().encode(new SecureRandom().generateSeed(20)));
    }
}
