create table g_role_permission
(
    role        varchar(255) references g_role (role),
    permission  varchar(255),
    primary key (role, permission)
);