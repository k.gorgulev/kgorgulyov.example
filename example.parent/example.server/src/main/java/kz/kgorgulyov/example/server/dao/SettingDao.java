package kz.kgorgulyov.example.server.dao;

import kz.kgorgulyov.example.share.model.Setting;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface SettingDao {

    @Select("select val from g_setting where  setting = #{set}  and server_name = #{sn} limit 1")
    String selSetting(@Param("set") Setting setting, @Param("sn") String serverName);

    @Insert("insert into g_setting (setting , val , server_name ) values(#{set} , #{val} , #{sn}) on conflict do nothing ;")
    void insSetting(@Param("set") Setting setting, @Param("val") String val, @Param("sn") String serverName);

    @Insert("update g_setting set val = #{val} where  server_name = #{sn} and setting = #{set} ")
    void updSetting(@Param("set") Setting setting, @Param("val") String val, @Param("sn") String serverName);

}
