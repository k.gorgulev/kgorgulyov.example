package kz.kgorgulyov.example.server.service;


import kz.kgorgulyov.example.share.model.Role;
import kz.kgorgulyov.example.share.model.RolePermission;

import java.util.List;

public interface UserRoleService {

    void createRole(Role role);

    List<RolePermission> getRolePermission(Role role);

    List<String> getRolePermissionString(Role role);

    void addPermission(Role role, RolePermission permission);

    void removePermission(Role role, RolePermission permission);


}
