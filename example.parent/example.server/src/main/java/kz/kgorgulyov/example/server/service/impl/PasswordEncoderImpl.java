package kz.kgorgulyov.example.server.service.impl;

import kz.kgorgulyov.example.server.service.PasswordEncoder;
import org.apache.commons.codec.binary.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordEncoderImpl implements PasswordEncoder {

    public static final String SALT_PWD_CONNECTOR = "";
    private final MessageDigest messageDigest;

    public PasswordEncoderImpl() {
        try {
            messageDigest = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String encode(String password, String salt) {
        return getSHA(password + SALT_PWD_CONNECTOR + salt);
    }

    @Override
    public boolean matches(String rawPassword, String encodedPassword) {
        return encodedPassword.equals(getSHA(rawPassword));
    }

    private synchronized String getSHA(String str) {
        messageDigest.update(str.getBytes());
        byte[] raw = messageDigest.digest();
        return (new Base64()).encodeToString(raw);
    }
}
