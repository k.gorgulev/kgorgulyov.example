package kz.kgorgulyov.example.share.model;

import java.util.List;

import static java.util.Arrays.asList;
import static kz.kgorgulyov.example.share.model.RolePermission.CAN;
import static kz.kgorgulyov.example.share.model.RolePermission.NOT_CAN;

public enum Role {
    ADMIN("Администратор", CAN, NOT_CAN),
    USER("Пользователь", CAN),
    ;
    public String description;
    public List<RolePermission> permissions;


    Role(String description, RolePermission... permissions) {
        this.description = description;
        this.permissions = asList(permissions);
    }

}

