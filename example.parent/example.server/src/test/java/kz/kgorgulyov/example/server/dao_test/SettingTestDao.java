package kz.kgorgulyov.example.server.dao_test;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SettingTestDao {

    @Delete("truncate g_setting")
    void delAll();
}
