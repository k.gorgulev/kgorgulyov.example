package kz.kgorgulyov.example.server.service;

import kz.kgorgulyov.example.server.dao.UserDao;
import kz.kgorgulyov.example.server.dao_test.UserTestDao;
import kz.kgorgulyov.example.server.util.BaseTest;
import kz.kgorgulyov.example.server.util.TestConst;
import kz.kgorgulyov.example.share.exception.InvalidFormException;
import kz.kgorgulyov.example.share.model.user.User;
import kz.kgorgulyov.example.share.model.user.dto.UserEditDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;

public class UserServiceTest extends BaseTest {

    @Autowired
    public UserService userService;

    @Autowired
    public UserDao userDao;

    @Autowired
    public UserTestDao userTestDao;

    @Autowired
    public PasswordEncoder passwordEncoder;


    @Test
    public void createUser() throws InvalidFormException {
        userTestDao.delAll();


        User user = new User();
        user.active = Boolean.TRUE;
        user.password = TestConst.DEFAULT_PASSWORD;
        user.login = randomAlphabetic(10);
        user.name = randomAlphabetic(10);
        user.surname = randomAlphabetic(10);
        user = userService.create(user);

        User userDB = userService.get(user.user);

        assertThat(user.login).isEqualTo(userDB.login);
        assertThat(user.name).isEqualTo(userDB.name);
        assertThat(user.surname).isEqualTo(userDB.surname);
    }

    @Test
    public void editUser() throws InvalidFormException {
        userTestDao.delAll();


        User user = new User();
        user.active = Boolean.TRUE;
        user.password = TestConst.DEFAULT_PASSWORD;
        user.login = randomAlphabetic(10);
        user.name = randomAlphabetic(10);
        user.surname = randomAlphabetic(10);
        user = userService.create(user);

        UserEditDto edit = new UserEditDto();
        edit.user = user.user;
        edit.name = randomAlphabetic(10);
        edit.surname = randomAlphabetic(10);
        userService.edit(edit);

        User userDB = userService.get(user.user);

        assertThat(userDB.name).isEqualTo(edit.name);
        assertThat(userDB.surname).isEqualTo(edit.surname);
    }

}
