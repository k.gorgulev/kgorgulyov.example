package kz.kgorgulyov.example.share.model.user.dto;


import kz.kgorgulyov.example.share.model.RolePermission;
import kz.kgorgulyov.example.share.model.user.User;
import kz.kgorgulyov.example.share.util.base.BaseDTO;

import java.util.ArrayList;
import java.util.List;

public class UserDto extends BaseDTO {

    public Long user;
    public String login;
    public String password;


    public String name;
    public String surname;
    public Boolean active;
    public String createdAt;
    public List<String> permissions;

    public UserDto(User user) {

        this.user = user.user;
        active = user.active;
        login = user.login;
        createdAt = date(user.createdAt);

        if (user.permissions != null) {
            permissions = new ArrayList<>();

            for (RolePermission permission : user.permissions) {
                permissions.add(permission.name());
            }
        }

    }

    public UserDto(User user, List<String> permissions) {


        login = user.login;
        name = user.name;
        surname = user.surname;
        active = user.active;
        createdAt = date(user.createdAt);

        this.permissions = permissions;
    }
}
