package kz.kgorgulyov.example.share.model.user.form;


import kz.kgorgulyov.example.share.util.base.BaseForm;

public class UserLoginForm extends BaseForm {

    public String login;
    public String password;

    public UserLoginForm() {
    }

    public UserLoginForm(String login, String password) {
        this.login = login;
        this.password = password;
    }

    @Override
    public void validateForm() {
        validateObjNotNull("login", login);
        validateObjNotNull("password", password);
    }

    @Override
    public String toString() {
        return "LoginForm{" + "login='" + login + '\'' + ", password='" + password + '\'' + '}';
    }
}
