package kz.kgorgulyov.example.server.service.impl;

import kz.kgorgulyov.example.server.dao.UserRoleDao;
import kz.kgorgulyov.example.server.service.UserRoleService;
import kz.kgorgulyov.example.share.model.Role;
import kz.kgorgulyov.example.share.model.RolePermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleDao userRoleDao;


    @Override
    public void createRole(Role role) {
        userRoleDao.insRole(role.name(), role.description);
    }

    @Override
    public List<RolePermission> getRolePermission(Role role) {
        List<RolePermission> ret = new ArrayList<>();
        for (String permission : userRoleDao.selPermissionOverRole(role.name())) {
            try {
                ret.add(RolePermission.valueOf(permission));
            } catch (Exception e) {
                throw new IllegalStateException(
                        "RolePermission со значением '" + permission + "' представлен в БД но не представлен в ENUM");
            }
        }
        return ret;

    }

    @Override
    public List<String> getRolePermissionString(Role role) {
        return userRoleDao.selPermissionOverRole(role.name());

    }

    @Override
    public void addPermission(Role role, RolePermission permission) {
        userRoleDao.insRolePermission(role.name(), permission.name());
    }

    @Override
    public void removePermission(Role role, RolePermission permission) {
        userRoleDao.delRolePermission(role.name(), permission.name());
    }


}
