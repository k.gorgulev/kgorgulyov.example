package kz.kgorgulyov.example.share.util;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtils {

    public static final String YEAR_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static String date2YearString(LocalDateTime datetime) {
        return date2string(datetime, YEAR_DATE_TIME_FORMAT);
    }

    public static String date2string(LocalDateTime datetime, String format) {
        if (datetime == null || format == null) {
            return null;
        }
        return datetime.format(DateTimeFormatter.ofPattern(format));
    }


}
