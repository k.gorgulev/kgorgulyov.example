package kz.kgorgulyov.example.server.bean;


import kz.kgorgulyov.example.server.bean.fake.PasswordEncoderFake;
import kz.kgorgulyov.example.server.service.PasswordEncoder;
import kz.kgorgulyov.example.server.service.SettingService;
import kz.kgorgulyov.example.server.service.impl.PasswordEncoderImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import static kz.kgorgulyov.example.share.model.Setting.DEV_MODE;

@Component
@DependsOn("liquibase")
public class PasswordEncoderFactory {

    @Autowired
    public SettingService settingService;

    @Bean
    public PasswordEncoder getPasswordEncoder() {

        if (settingService.getSettingBool(DEV_MODE)) {
            return new PasswordEncoderFake();
        }

        return new PasswordEncoderImpl();
    }

}
