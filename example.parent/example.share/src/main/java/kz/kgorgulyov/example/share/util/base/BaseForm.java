package kz.kgorgulyov.example.share.util.base;


import kz.kgorgulyov.example.share.exception.InvalidFormException;
import kz.kgorgulyov.example.share.util.DateUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public abstract class BaseForm<T extends Enum<T>> implements Serializable {

    protected List<String> validationErrors = new ArrayList<>();

    public void validate() throws InvalidFormException {
        validateForm();
        if (validationErrors != null && !validationErrors.isEmpty()) {
            throw new InvalidFormException(validationErrors);
        }
    }

    /**
     * @return list of validation errors
     */

    public abstract void validateForm() throws InvalidFormException;

    public List<String> validationErrors() {
        return validationErrors;
    }

    /**
     * @param eEnum - class of Enum
     * @param val   - value of string that must be enum
     */

    public boolean validateEnum(Class<T> eEnum, String title, String val) {
        if (val == null) {
            validationErrors.add(
                    "missed " + title + " one of values '" + Arrays.asList(eEnum.getEnumConstants()) + "'");
            return false;
        }
        try {
            eEnum.getField(val);
            return true;
        } catch (NoSuchFieldException e) {
            validationErrors.add(
                    "invalid value " + title + " - '" + val + "' " + Arrays.asList(eEnum.getEnumConstants()));
            return false;
        }
    }

    /**
     * @param title - title to display in response
     * @param val   - value
     */
    public boolean validateStr(String title, String val) {
        if (val == null || val.isEmpty()) {
            validationErrors.add("missed value '" + title + "'");
            return false;
        }
        return true;
    }

    /**
     * @param title - title to display in response
     * @param val   - value
     */
    public void validateStrLen(String title, String val, int len) {
        if (val == null || val.isEmpty()) {
            validationErrors.add("missed value '" + title + "'");
        }
        if (val.length() != len) {
            validationErrors.add("value length actual: " + val.length() + " expected " + len);
        }
    }


    /**
     * @param title - title to display in response
     * @param val   - value
     */
    public void validateNumber(String title, String val) {
        try {
            new BigDecimal(val);
        } catch (Exception e) {
            validationErrors.add("invalid value '" + title + "'");
        }
    }


    public void validateBool(String title, String val) {
        try {
            Boolean.valueOf(val);
        } catch (NumberFormatException e) {
            validationErrors.add("invalid value '" + title + "'");
        }
    }

    /**
     * @param title - title to display in response
     * @param val   - value
     * @return true if list valid, else false
     */
    public boolean validateList(String title, List<?> val) {
        if (val == null || val.isEmpty()) {
            validationErrors.add("missed value '" + title + "'");
            return false;
        }
        return true;
    }

    public boolean validateMap(String title, Map<?, ?> val) {
        if (val == null || val.isEmpty()) {
            validationErrors.add("missed value '" + title + "'");
            return false;
        }
        return true;
    }

    /**
     * @param title - title to display in response
     * @param val   - value
     * @return true if list valid, else false
     */
    public boolean validateListSize(String title, List<?> val, int size) {
        if (val == null || val.isEmpty()) {
            validationErrors.add("missed value '" + title + "'");
            return false;
        }
        if (val.size() != size) {
            validationErrors.add("wrong value size '" + title + "'");
            return false;
        }
        return true;
    }


    public boolean validateMapSize(String title, Map<?, ?> val, int size) {
        if (val == null || val.isEmpty()) {
            validationErrors.add("missed value '" + title + "'");
            return false;
        }
        if (val.size() != size) {
            validationErrors.add("wrong value size '" + title + "'");
            return false;
        }
        return true;
    }

    /**
     * @param title - title to display in response
     * @param val   - value
     * @return true if list valid, else false
     */
    public boolean validateListSizeLarger(String title, List<?> val, int size) {
        if (val == null || val.isEmpty()) {
            validationErrors.add("missed value '" + title + "'");
            return false;
        }
        if (val.size() > size) {
            validationErrors.add("wrong value size '" + title + "'");
            return false;
        }
        return true;
    }

    public boolean validateMapSizeLarger(String title, Map<?, ?> val, int size) {
        if (val == null || val.isEmpty()) {
            validationErrors.add("missed value '" + title + "'");
            return false;
        }
        if (val.size() > size) {
            validationErrors.add("wrong value size '" + title + "'");
            return false;
        }
        return true;
    }


    /**
     * @param title - title to display in response
     * @param val   - value
     */
    public boolean validateObjNotNull(String title, Object val) {
        if (val == null) {

            validationErrors.add("missed value '" + title + "'");
            return false;
        }
        if (val instanceof String && ((String) val).isEmpty()) {
            validationErrors.add("missed value '" + title + "'");
            return false;
        }
        return true;
    }

    public boolean validateLocalDateTime(String title, String val) {
        try {
            LocalDateTime.parse(val);
            return true;
        } catch (Exception e) {
            validationErrors.add("invalid value '" + title + "' ");
            return false;
        }
    }

    public boolean isNotNullObj(Object val) {
        if (val != null && val instanceof String) {
            return !((String) val).isEmpty();
        }
        return val != null;
    }

    public boolean isNotNullList(List<?> val) {
        if (val == null || val.isEmpty()) {
            return false;
        }
        return true;
    }

    public boolean isNotNullMap(Map<?, ?> val) {
        if (val == null || val.isEmpty()) {
            return false;
        }
        return true;
    }


    protected String date(LocalDateTime d) {
        return DateUtils.date2YearString(d);
    }

}
