package kz.kgorgulyov.example.server.util.security;

import kz.kgorgulyov.example.server.service.UserService;
import kz.kgorgulyov.example.server.util.HandlerExceptionToViewResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.servlet.HandlerExceptionResolver;

@Configuration
@EnableWebSecurity(debug = false)
@ComponentScan(value = {"kz.kgorgulyov.example.server"})
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserService userService;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().mvcMatchers("/public/*").permitAll()
                .mvcMatchers("/errors").permitAll().anyRequest().authenticated().and().csrf().disable();


    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
        auth.authenticationProvider(new CustomAuthProvider(userService));
    }


    @Bean
    HandlerExceptionResolver customExceptionResolver() {
        return new HandlerExceptionToViewResolver();
    }

}
