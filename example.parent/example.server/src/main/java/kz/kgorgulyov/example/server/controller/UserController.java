package kz.kgorgulyov.example.server.controller;


import kz.kgorgulyov.example.server.service.AuthService;
import kz.kgorgulyov.example.server.service.UserService;
import kz.kgorgulyov.example.server.util.security.SessionContext;
import kz.kgorgulyov.example.share.exception.InvalidFormException;
import kz.kgorgulyov.example.share.model.LoggerType;
import kz.kgorgulyov.example.share.model.user.form.UserEditForm;
import kz.kgorgulyov.example.share.util.rest.RestResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    public UserService userService;

    @Autowired
    public AuthService authService;


    public org.apache.log4j.Logger LOGGER = Logger.getLogger(LoggerType.PUBLIC_API.logger);

    @PreAuthorize("hasAnyAuthority('NOT_CAN')")
    @PutMapping
    public RestResponse edit(@RequestBody UserEditForm form) throws InvalidFormException {
        form.validateForm();
        return RestResponse.ok().addData(userService.edit(form.toUserEditDto(SessionContext.userId())));

    }

    @PreAuthorize("hasAnyAuthority('CAN')")
    @DeleteMapping
    public RestResponse delete() {
        userService.setActive(SessionContext.userId(), false);
        return RestResponse.ok();

    }


    @RequestMapping(path = "/logout", method = RequestMethod.GET)
    public RestResponse logoutGet(HttpServletRequest request) {
        new SecurityContextLogoutHandler().logout(request, null, null);
        return RestResponse.ok();
    }


}
