package kz.kgorgulyov.example.server.dao;

import kz.kgorgulyov.example.share.model.user.User;
import kz.kgorgulyov.example.share.model.user.dto.UserEditDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface UserDao {

    @Select("select nextval('seq_g_user_user');")
    Long seq();

    User selByLogin(@Param("login") String login);

    User selById(@Param("id") Long userId);


    void mrg(@Param("u") User user);


    @Select("select exists (select 1 from g_user where login = #{l} and active)")
    boolean isExistLogin(@Param("l") String login);


    void updNameAndSurname(@Param("d") UserEditDto toUser);

    @Update("update g_user set active = #{a} where userid = #{id}")
    void updActive(@Param("id") Long userId, @Param("a") Boolean active);
}
