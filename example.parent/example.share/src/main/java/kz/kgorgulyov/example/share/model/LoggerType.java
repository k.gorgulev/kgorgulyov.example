package kz.kgorgulyov.example.share.model;

public enum LoggerType {

    PUBLIC_API("c_public_api"),
    SCHEDULER("c_scheduler"),

    ;
    public String logger;

    LoggerType(String logger) {
        this.logger = logger;
    }
}
