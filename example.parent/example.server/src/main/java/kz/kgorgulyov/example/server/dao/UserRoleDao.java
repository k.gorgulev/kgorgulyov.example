package kz.kgorgulyov.example.server.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserRoleDao {

    @Insert("insert into g_role (role, description) values (#{name} , #{desc}) on conflict do nothing; ")
    void insRole(@Param("name") String roleName, @Param("desc") String roleDesc);

    @Insert("insert INTO g_role_permission(role, permission) values (#{role} , #{permission}) on conflict do nothing; ")
    void insRolePermission(@Param("role") String role, @Param("permission") String permission);

    @Insert("delete from g_role_permission where role = #{role} and  permission = #{permission}; ")
    void delRolePermission(@Param("role") String role, @Param("permission") String permission);

    @Select("select permission from g_role_permission where role = #{role} order by permission")
    List<String> selPermissionOverRole(@Param("role") String role);

}
