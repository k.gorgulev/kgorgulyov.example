package kz.kgorgulyov.example.server.service;

import kz.kgorgulyov.example.server.dao_test.SettingTestDao;
import kz.kgorgulyov.example.server.util.BaseTest;
import kz.kgorgulyov.example.share.model.Setting;
import org.assertj.core.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

public class SettingServiceTest extends BaseTest {

    @Autowired
    public SettingService settingService;


    @Autowired
    public InitService initService;

    @Autowired
    public SettingTestDao settingTestDao;


    @Test
    public void storeAndTestBool() {
        settingTestDao.delAll();

        initService.init();

        settingService.insSetting(Setting.DEV_MODE, Boolean.TRUE.toString());
        boolean settingBool = settingService.getSettingBool(Setting.DEV_MODE);
        Assertions.assertThat(settingBool).isTrue();

        settingService.updSetting(Setting.DEV_MODE, Boolean.FALSE.toString());
        settingBool = settingService.getSettingBool(Setting.DEV_MODE);
        Assertions.assertThat(settingBool).isFalse();
    }

}
