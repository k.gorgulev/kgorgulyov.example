package kz.kgorgulyov.example.server.service;

import javax.annotation.PostConstruct;

public interface InitService {
    @PostConstruct
    void init();
}
