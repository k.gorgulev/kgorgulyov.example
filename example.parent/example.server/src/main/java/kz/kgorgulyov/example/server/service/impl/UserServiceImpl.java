package kz.kgorgulyov.example.server.service.impl;


import kz.kgorgulyov.example.server.dao.UserDao;
import kz.kgorgulyov.example.server.dao.UserRoleDao;
import kz.kgorgulyov.example.server.service.PasswordEncoder;
import kz.kgorgulyov.example.server.service.SettingService;
import kz.kgorgulyov.example.server.service.UserRoleService;
import kz.kgorgulyov.example.server.service.UserService;
import kz.kgorgulyov.example.share.exception.InvalidFormException;
import kz.kgorgulyov.example.share.model.Role;
import kz.kgorgulyov.example.share.model.user.User;
import kz.kgorgulyov.example.share.model.user.dto.UserEditDto;
import kz.kgorgulyov.example.share.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


@Component
public class UserServiceImpl implements UserService {

    @Autowired
    public UserDao userDao;

    @Autowired
    public UserRoleService userRoleService;

    @Autowired
    public UserRoleDao userRoleDao;

    @Autowired
    public PasswordEncoder passwordEncoder;

    @Autowired
    public SettingService settingService;

    @Override
    public User login(String login, String password) {
        User user = userDao.selByLogin(login);
        if (user != null) {
            if (passwordEncoder.encode(password, user.salt).equals(user.password)) {
                user.permissions.addAll(userRoleService.getRolePermission(user.role));
                return user;
            }
        }
        return null;
    }


    @Override
    public User create(User user) throws InvalidFormException {
        if (userDao.isExistLogin(user.login)) {
            throw new InvalidFormException(List.of("login already exists"));
        }

        user.user = userDao.seq();
        user.salt = CommonUtil.getSalt();
        user.password = passwordEncoder.encode(user.password, user.salt);
        user.createdAt = LocalDateTime.now();
        user.active = true;
        user.role = Role.USER;

        userDao.mrg(user);

        return user;
    }

    @Override
    public User edit(UserEditDto toUser) {
        userDao.updNameAndSurname(toUser);
        return userDao.selById(toUser.user);
    }

    @Override
    public User get(Long user) {
        return userDao.selById(user);
    }

    @Override
    public void setActive(Long userId, Boolean b) {
        userDao.updActive(userId, b);
    }


}
