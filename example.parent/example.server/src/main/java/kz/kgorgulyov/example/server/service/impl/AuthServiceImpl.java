package kz.kgorgulyov.example.server.service.impl;

import kz.kgorgulyov.example.server.dao.UserDao;
import kz.kgorgulyov.example.server.service.AuthService;
import kz.kgorgulyov.example.server.service.PasswordEncoder;
import kz.kgorgulyov.example.server.service.UserRoleService;
import kz.kgorgulyov.example.server.util.security.CustomAuthProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

@Component
public class AuthServiceImpl implements AuthService {

    @Autowired
    private CustomAuthProvider authProvider;


    @Autowired
    public PasswordEncoder passwordEncoder;

    @Autowired
    public UserRoleService userRoleService;


    @Autowired
    public UserDao userDao;

    @Override
    public Authentication auth(HttpSession session, String login, String password) {
        UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(login, password);
        Authentication auth = authProvider.authenticate(authReq);
        SecurityContext sc = SecurityContextHolder.getContext();
        sc.setAuthentication(auth);

        session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, sc);
        return auth;
    }


}
