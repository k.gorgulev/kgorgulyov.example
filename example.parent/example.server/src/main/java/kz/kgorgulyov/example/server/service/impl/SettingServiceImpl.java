package kz.kgorgulyov.example.server.service.impl;

import kz.kgorgulyov.example.server.dao.SettingDao;
import kz.kgorgulyov.example.server.service.SettingService;
import kz.kgorgulyov.example.share.model.Setting;
import kz.kgorgulyov.example.share.util.CommonConst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static kz.kgorgulyov.example.share.util.CommonUtil.nvl;


@Component
public class SettingServiceImpl implements SettingService {

    @Autowired
    public SettingDao settingDao;

    @Override
    public boolean getSettingBool(Setting setting) {
        return Boolean.valueOf(getValue(setting));
    }

    @Override
    public String getSettingString(Setting setting) {
        return getValue(setting);
    }

    @Override
    public int getSettingInteger(Setting setting) {
        return Integer.valueOf(getValue(setting));
    }


    public String getValue(Setting setting) {
        String val = settingDao.selSetting(setting, getHost());
        if (val == null) {
            String defSetting = settingDao.selSetting(setting, CommonConst.DEFAULT_HOST);
            if (defSetting == null) {
                insSetting(setting, setting.defaultVal);
                return setting.defaultVal;
            } else {
                insSetting(setting, defSetting);
                return defSetting;
            }

        }
        return val;

    }

    public String getHost() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            return CommonConst.DEFAULT_HOST;
        }
    }

    @Override
    public void insSetting(Setting setting, String val) {
        settingDao.insSetting(setting, nvl(val), getHost());
    }

    @Override
    public void updSetting(Setting setting, String val) {
        settingDao.updSetting(setting, nvl(val), getHost());
    }


    public void initDefault() {
        for (Setting setting : Setting.values()) {
            getValue(setting);
        }
    }
}