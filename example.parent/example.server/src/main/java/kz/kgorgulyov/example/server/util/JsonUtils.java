package kz.kgorgulyov.example.server.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class JsonUtils {
    public static String getObjectJson(Object o) {
        String s = null;
        try {
            s = new Gson().toJson(o);
        } catch (Exception e) {
            e.printStackTrace();
            //remoteLogger.sendError(e);
        }
        return s;
    }


    public static String toPretty(String json) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(json);
        return gson.toJson(je);
    }
}