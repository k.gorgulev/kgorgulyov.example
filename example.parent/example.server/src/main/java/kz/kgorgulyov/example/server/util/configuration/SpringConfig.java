package kz.kgorgulyov.example.server.util.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@ComponentScan(basePackages = "kz.kgorgulyov.example.server.controller")
@EnableWebMvc
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SpringConfig extends WebMvcConfigurerAdapter {


}

