package kz.kgorgulyov.example.server.service;

import kz.kgorgulyov.example.share.model.Setting;

public interface SettingService {
    boolean getSettingBool(Setting setting);

    String getSettingString(Setting setting);

    int getSettingInteger(Setting setting);

    void insSetting(Setting setting, String val);

    void updSetting(Setting setting, String val);


}
