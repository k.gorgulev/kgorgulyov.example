package kz.kgorgulyov.example.server.bean.fake;


import kz.kgorgulyov.example.server.service.PasswordEncoder;

public class PasswordEncoderFake implements PasswordEncoder {
    @Override
    public String encode(String password, String salt) {
        return password;
    }

    @Override
    public boolean matches(String password, String encodedPassword) {
        return password.equals(encodedPassword);
    }
}
