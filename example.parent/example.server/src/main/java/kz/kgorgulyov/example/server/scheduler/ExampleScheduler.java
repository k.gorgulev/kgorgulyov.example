package kz.kgorgulyov.example.server.scheduler;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import static kz.kgorgulyov.example.share.model.LoggerType.SCHEDULER;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class ExampleScheduler {

    public org.apache.log4j.Logger LOGGER_SCHEDULER = Logger.getLogger(SCHEDULER.logger);

    public void log(String msg){
        LOGGER_SCHEDULER.info(msg);
    }

    @Scheduled(cron = "0  */1 * * * ?")
    public void publish() {
        log("Start");
        //Some code
        log("End");
    }

}
