package kz.kgorgulyov.example.server.service;

public interface PasswordEncoder {

    String encode(String password, String salt);

    boolean matches(String password, String encodedPassword);
}
