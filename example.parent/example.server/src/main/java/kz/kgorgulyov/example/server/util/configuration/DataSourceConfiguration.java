package kz.kgorgulyov.example.server.util.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import kz.kgorgulyov.example.share.util.CommonConst;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Scanner;

import static org.apache.commons.io.FileUtils.writeStringToFile;

@Configuration()
public class DataSourceConfiguration {

    public final String LOGIN = "login";
    public final String PASSWORD = "password";
    public final String URL = "url";


    private void log(String msg) {
        System.out.println(msg);
    }

    @Bean("hikariDataSource")
    public HikariDataSource hikariDataSource() throws IOException {
        DatabaseConf conf = new DatabaseConf();

        initConfig(conf);

        log("***********************************");
        log("DB properties:");
        log("path=" + conf.path);
        log("login=" + conf.login);
        log("password=" + conf.password);
        log("url=" + conf.url);
        log("***********************************");

        HikariConfig config = new HikariConfig();
        config.setDriverClassName("org.postgresql.Driver");
        config.setJdbcUrl(conf.url);
        config.setUsername(conf.login);
        config.setPassword(conf.password);

        config.setMaximumPoolSize(100);


        return new HikariDataSource(config);
    }

    private DatabaseConf initConfig(DatabaseConf dbConf) throws IOException {
        String pathToConf = System.getProperty("user.home") + "/" + CommonConst.PROJECT + ".d";

        File folder = new File(pathToConf);
        boolean loginEx = false, passEx = false, urlEx = false;


        if (!folder.exists()) {
            folder.mkdir();
            new File(pathToConf + "/logs").mkdir();
        }
        File conf = new File(pathToConf + "/postgres.prop");

        if (!conf.exists()) {
            conf.createNewFile();
            writeStringToFile(conf, LOGIN + "=" + dbConf.login + "\n", Charset.defaultCharset(), true);
            writeStringToFile(conf, PASSWORD + "=" + dbConf.password + "\n", Charset.defaultCharset(), true);
            writeStringToFile(conf, URL + "=" + dbConf.url + "\n", Charset.defaultCharset(), true);
            return dbConf;

        }
        Scanner sc = new Scanner(new FileReader(conf));


        while (sc.hasNext()) {
            String line = sc.nextLine();
            String[] split = line.split("=");
            if (split.length < 2) {
                continue;
            }
            if (line.startsWith(LOGIN + "=")) {
                dbConf.login = line.substring((LOGIN + "=").length());
                loginEx = true;
                continue;
            }
            if (line.startsWith(PASSWORD + "=")) {
                dbConf.password = line.substring((PASSWORD + "=").length());
                passEx = true;
                continue;
            }
            if (line.startsWith(URL + "=")) {
                dbConf.url = line.substring((URL + "=").length());
                urlEx = true;
            }

        }

        if (!passEx || !urlEx || !loginEx) {
            if (!loginEx) {
                writeStringToFile(conf, LOGIN + "=" + dbConf.login + "\n", Charset.defaultCharset(), true);
            }
            if (!passEx) {
                writeStringToFile(conf, PASSWORD + "=" + dbConf.password + "\n", Charset.defaultCharset(), true);
            }
            if (!urlEx) {
                writeStringToFile(conf, URL + "=" + dbConf.url + "\n", Charset.defaultCharset(), true);
            }
        }

        dbConf.path = pathToConf;
        return dbConf;
    }


}
