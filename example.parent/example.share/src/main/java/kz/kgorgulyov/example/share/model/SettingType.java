package kz.kgorgulyov.example.share.model;

public enum SettingType {
    STRING,
    INTEGER,
    BOOL,
    DATE

}
