package kz.kgorgulyov.example.share.model;

import static kz.kgorgulyov.example.share.model.SettingType.BOOL;

public enum Setting {

    DEV_MODE(BOOL, Boolean.TRUE.toString()),

    ;

    public SettingType type;
    public String defaultVal;
    public String title;

    Setting(SettingType type, String defaultVal, String title) {
        this.type = type;
        this.defaultVal = defaultVal;
        this.title = title;
    }

    Setting(SettingType type, String defaultVal) {
        this.type = type;
        this.defaultVal = defaultVal;
    }


}
