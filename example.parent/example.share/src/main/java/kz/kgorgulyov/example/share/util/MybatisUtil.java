package kz.kgorgulyov.example.share.util;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class MybatisUtil {

    public static Long getLong(Object o) {
        Number number = (Number) o;
        if (number == null) { return null; }
        return Long.valueOf(number.longValue());
    }

    public static LocalDateTime getLocalDateTime(Object o) {
        Timestamp timestamp = (Timestamp) o;
        if (timestamp == null) { return null; }
        return timestamp.toLocalDateTime();
    }

    public static String str(Object o) {
        return (String) o;
    }

}
