package kz.kgorgulyov.example.share.model.user.form;


import kz.kgorgulyov.example.share.model.user.dto.UserEditDto;
import kz.kgorgulyov.example.share.util.base.BaseForm;

public class UserEditForm extends BaseForm {


    public String name;
    public String surname;


    @Override
    public void validateForm() {
        if (isNotNullObj(name) && isNotNullObj(name)) {
            validationErrors.add("missed name and surname");
        } else {
            validateStr("name", name);
            validateStr("surname", surname);
        }


    }


    public UserEditDto toUserEditDto(Long user) {
        UserEditDto u = new UserEditDto();
        u.user = user;
        if (isNotNullObj(name)) {
            u.name = name;
        }
        if (isNotNullObj(surname)) {
            u.surname = surname;
        }
        return u;

    }


}

