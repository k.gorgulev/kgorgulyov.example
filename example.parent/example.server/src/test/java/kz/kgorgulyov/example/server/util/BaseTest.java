package kz.kgorgulyov.example.server.util;

import kz.kgorgulyov.example.server.service.InitService;
import kz.kgorgulyov.example.server.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;

@WebAppConfiguration
@ContextConfiguration("classpath:test_context.xml")
public class BaseTest extends AbstractTestNGSpringContextTests {


    @Autowired
    public InitService initService;

    @Autowired
    public SettingService settingService;


    public void init() {
        initService.init();
    }
}
