package kz.kgorgulyov.example.server.service;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpSession;

public interface AuthService {

    Authentication auth(HttpSession session, String login, String password);
}
