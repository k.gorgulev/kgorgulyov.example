package kz.kgorgulyov.example.share.exception;

import java.util.List;

public class InvalidFormException extends Exception {
    public List<String> invalidFields;


    public InvalidFormException(List<String> invalidFields) {
        super(getErrMsg(invalidFields));
        this.invalidFields = invalidFields;
    }

    private static String getErrMsg(List<String> errors) {
        if (errors == null || errors.isEmpty()) {
            return "error msg not set";
        }
        if (errors.size() == 1) {
            return errors.get(0);
        }
        String ret = "";
        for (String error : errors) {
            ret += error + "\n";
        }
        return ret;
    }


}
