package kz.kgorgulyov.example.server.util.configuration;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan({"kz.kgorgulyov.example.server.dao_test"})
public class MybatisTestConfiguration {
}
