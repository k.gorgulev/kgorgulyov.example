package kz.kgorgulyov.example.server.controller;


import kz.kgorgulyov.example.share.exception.InvalidURLException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ErrorController {


    @RequestMapping(value = "errors", method = RequestMethod.GET)
    public void renderErrorPage(HttpServletRequest httpRequest) throws InvalidURLException {
        throw new InvalidURLException(getErrorCode(httpRequest));

    }

    private int getErrorCode(HttpServletRequest httpRequest) {
        return (Integer) httpRequest.getAttribute("javax.servlet.error.status_code");
    }

}
