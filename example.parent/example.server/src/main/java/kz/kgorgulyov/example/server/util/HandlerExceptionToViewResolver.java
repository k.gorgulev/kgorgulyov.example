package kz.kgorgulyov.example.server.util;


import kz.kgorgulyov.example.share.exception.InvalidFormException;
import kz.kgorgulyov.example.share.exception.InvalidURLException;
import kz.kgorgulyov.example.share.util.rest.RestResponse;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HandlerExceptionToViewResolver implements HandlerExceptionResolver {
    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
            Exception ex) {

        if (ex instanceof InvalidFormException) {
            try {
                InvalidFormException exception = (InvalidFormException) ex;
                PrintWriter out = response.getWriter();
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                out.print(JsonUtils.getObjectJson(RestResponse.validationError(exception.invalidFields)));
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();

            }
            return new ModelAndView();
        } else if (ex instanceof InvalidURLException) {
            InvalidURLException exception = (InvalidURLException) ex;

            String errorMsg = "";

            switch (exception.code) {
                case 400: {
                    errorMsg += "Http Error Code: 400. Bad Request";
                    break;
                }
                case 401: {
                    errorMsg += "Http Error Code: 401. Unauthorized";
                    break;
                }
                case 403: {
                    errorMsg += "Http Error Code: 403. Unauthorized";
                    break;
                }
                case 404: {
                    errorMsg += "Http Error Code: 404. Resource not found";
                    break;
                }
                case 500: {
                    errorMsg += "Http Error Code: 500. Internal Server Error";
                    break;
                }
            }
            try {
                PrintWriter out = response.getWriter();
                response.setContentType("text/html");
                response.setStatus(exception.code);
                response.setCharacterEncoding("UTF-8");
                out.print(errorMsg);
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return new ModelAndView();
        }
        return null;
    }


    private int getErrorCode(HttpServletRequest httpRequest) {
        return (Integer) httpRequest.getAttribute("javax.servlet.error.status_code");
    }
}
