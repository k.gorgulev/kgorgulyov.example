create sequence seq_g_user_user;

create table g_user
(
    userid     bigint primary key,
    login      text not null,
    password   text not null,
    salt       text not null,
    active     bool      default true,

    role       varchar(255) references g_role (role),

    name       text,
    surname    text,
    created_at timestamp default now()
);

create index i_g_user_login on g_user (login);
create index i_g_user_active on g_user (active);
create index i_g_user_created_at on g_user (created_at);