package kz.kgorgulyov.example.server.service;

import kz.kgorgulyov.example.share.exception.InvalidFormException;
import kz.kgorgulyov.example.share.model.user.User;
import kz.kgorgulyov.example.share.model.user.dto.UserEditDto;

public interface UserService {

    User login(String login, String password);

    User create(User user) throws InvalidFormException;

    User edit(UserEditDto toUser);


    User get(Long user);

    void setActive(Long userId, Boolean b);
}