package kz.kgorgulyov.example.server.service.impl;

import kz.kgorgulyov.example.server.service.InitService;
import kz.kgorgulyov.example.server.service.UserRoleService;
import kz.kgorgulyov.example.share.model.Role;
import kz.kgorgulyov.example.share.model.RolePermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@DependsOn("liquibase")
public class InitServiceImpl implements InitService {


    private void log(String msg) {
        System.out.println(msg);
    }


    @Autowired
    private UserRoleService userRoleService;

    @Override
    @PostConstruct
    public void init() {
        log("****************************************");
        log("****************************************");
        log("****************************************");
        log("Init started");

        for (Role value : Role.values()) {
            userRoleService.createRole(value);
            for (RolePermission permission : value.permissions) {
                userRoleService.addPermission(value, permission);
            }
        }

        log("****************************************");
        log("****************************************");
    }


}
