package kz.kgorgulyov.example.share.exception;

public class InvalidURLException extends Exception {
    public Integer code;

    public InvalidURLException(Integer code) {
        this.code = code;
    }
}
