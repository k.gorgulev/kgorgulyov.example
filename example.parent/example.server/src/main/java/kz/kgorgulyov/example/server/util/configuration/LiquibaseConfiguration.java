package kz.kgorgulyov.example.server.util.configuration;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Component
public class LiquibaseConfiguration {


    @Autowired
    private DataSource dataSource;

    @Autowired
    private ResourceLoader resourceLoader;

    @Bean(name = "liquibase")
    public SpringLiquibase liquibase() throws Exception {
        //      Locate change log file
        String changelogFile = "classpath:liquibase/changelog.xml";
        Resource resource = resourceLoader.getResource(changelogFile);

        Assert.state(resource.exists(), "Unable to find file: " + resource.getFilename());
        // Configure Liquibase
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setChangeLog(changelogFile);
        liquibase.setDataSource(dataSource);
        liquibase.setDropFirst(false);
        liquibase.setShouldRun(true);

        // Verbose logging
        Map<String, String> params = new HashMap<>();
        params.put("verbose", "false");
        return liquibase;
    }
}