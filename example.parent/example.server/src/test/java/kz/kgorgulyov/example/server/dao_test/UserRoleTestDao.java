package kz.kgorgulyov.example.server.dao_test;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserRoleTestDao {

    @Delete("truncate g_role_permission;")
    void delPermission();
}
